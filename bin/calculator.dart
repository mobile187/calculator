import 'dart:io';

import 'package:calculator/calculator.dart' as calculator;

void main(List<String> arguments) {
  double numX = 0;
  double result = 0;

  String? opMenu = ' ';

  optionProcess(numX, opMenu, result);
}

void optionProcess(double numX, String? opMenu, double result) {
  print('Input number: ');
  numX = double.parse(stdin.readLineSync()!);

  while (true) {
    print('1.Add\n2.Subtract\n3.Multiply\n4.Divide\n5.Clear\n6.Exit');
    print('Select number of operator or menu: ');
    opMenu = stdin.readLineSync();

    if (opMenu == '1') {
      print('Input number: ');
      result = numX + double.parse(stdin.readLineSync()!);
      numX = result;
      showResult(result);
    } else if (opMenu == '2') {
      print('Input number: ');
      result = numX - double.parse(stdin.readLineSync()!);
      numX = result;
      showResult(result);
    } else if (opMenu == '3') {
      print('Input number: ');
      result = numX * double.parse(stdin.readLineSync()!);
      numX = result;
      showResult(result);
    } else if (opMenu == '4') {
      print('Input number: ');
      result = numX / double.parse(stdin.readLineSync()!);
      numX = result;
      showResult(result);
    } else if (opMenu == '5') {
      result = 0;
      showResult(result);
      print('Input number: ');
      numX = double.parse(stdin.readLineSync()!);
    } else if (opMenu == '6') {
      exit(0);
    }
  }
}

void showResult(double result) {
  print('Result: ');
  print(result);
  print('__________________');
}
